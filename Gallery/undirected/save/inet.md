---
redirect_from:
  - /_pages/Gallery/undirected/save/inet.html
layout: gallery
title: Intranet Layout
svg: inet.svg
copyright: Copyright &#169; 1996 AT&amp;T.  All rights reserved.
gv_file: inet.dot.txt
img_src: inet.png
---
This is the graph of an intranet.
The edge length adjustment in the last line
is an attempt to improve the placement of
two nodes that otherwise wind up too close.
Notice the way of using subgraphs to create
a fully-connected subgraph between the nodes
`IHP`, `IW`, `IH1`, `IH2`, and `IH4`.
