---
name: addPoint
---
A [point](#k:point) with an optional prefix `'+'`.
