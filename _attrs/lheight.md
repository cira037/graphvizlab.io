---
defaults: []
flags:
- write
minimums: []
name: lheight
types:
- double
used_by: GC
---
Height of graph or cluster label, in inches.
