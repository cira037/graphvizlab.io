---
defaults:
- '1'
flags:
- dot
minimums:
- '0'
name: minlen
types:
- int
used_by: E
---
Minimum edge length (rank difference between head and tail).
