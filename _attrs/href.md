---
defaults:
- '""'
flags:
- map
- postscript
- svg
minimums: []
name: href
types:
- escString
used_by: GCNE
---
Synonym for [`URL`](#d:URL).
