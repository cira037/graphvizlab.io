---
defaults:
- center
flags: []
minimums: []
name: tailport
types:
- portPos
used_by: E
---
Indicates where on the tail node to attach the tail of the edge.

See [limitation](#h:undir_note).
