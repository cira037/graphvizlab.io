---
defaults:
- 'false'
flags: []
minimums: []
name: labelfloat
types:
- bool
used_by: E
---
If true, allows edge labels to be less constrained in position.
In particular, it may appear on top of other edges.
