---
defaults:
- '"none"'
flags:
- sfdp
minimums: []
name: smoothing
types:
- smoothType
used_by: G
---
Specifies a post-processing step used to smooth out an uneven distribution 
of nodes.
